#!/usr/bin/perl

use strict;
use warnings;

use lib '/usr/src/apt-cacher/src/lib';

require('apt-cacher.pl');

my $cfg = read_config('/dev/null'); # Get defaults with no config file
my $re = join ' ', $cfg->{ubuntu_release_names};

open(my $input, '<', '/usr/src/apt-cacher/src/debian/apt-cacher.8') || die $!;

while (<$input>) {
    if (/$re/) {
	print "$\n";
	next;
    }
    foreach my $key (keys %$cfg) {
	next unless $key =~ 'regexp';
#        s/^.BI? "?$key\s+\[.*$/.B $key [$cfg->{$key}]/
        s/^.BI? "?$key\s+\[.*$/.BI "$key [" "see default \/etc\/apt\-cacher\/apt\-cacher.conf" "]"/
	  && s/\\/\\(rs/g # groff printable backslashes
	  && s/\|/|\\:/g # Insert groff line break positions
	    && last;
    }
    print;
}
