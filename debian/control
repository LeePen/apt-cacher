Source: apt-cacher
Section: net
Priority: optional
Maintainer: Mark Hindley <mark@hindley.org.uk>
Uploaders: Eduard Bloch <blade@debian.org>
Build-Depends: debhelper-compat (= 13), po-debconf
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/LeePen/apt-cacher.git
Vcs-Browser: https://salsa.debian.org/LeePen/apt-cacher

Package: apt-cacher
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: distro-info-data,
         ed,
         libdpkg-perl,
         libfilesys-df-perl,
	 libio-compress-lzma-perl,
         libio-interactive-perl,
         libio-interface-perl,
         libipc-sharelite-perl,
         libnetaddr-ip-perl,
         libsys-syscall-perl,
         libwww-curl-perl,
         libwww-perl,
         update-inetd,
         ${misc:Depends},
         ${perl:Depends},
Recommends: libberkeleydb-perl
Suggests: libfreezethaw-perl, libio-socket-inet6-perl
Description: Caching proxy server for Debian/Ubuntu/Devuan software repositories
 Apt-cacher performs caching of files requested by apt-get (or other APT clients
 such as aptitude or synaptic). Apt-cacher can also proxy Debian Bugs SOAP
 requests for apt-listbugs. It is most useful for local area networks with
 limited network bandwidth or to reduce multiple large downloads.
 .
 When a file or package is requested, apt-cacher checks whether it already has
 the requested version, in which case it fulfils the request immediately.  If
 not, it downloads the package while streaming it to the client at the same
 time. A local copy is then kept for any subsequent requests.
 .
 Apt-cacher has been optimized for best utilization of network bandwidth and is
 efficient even on slow or low-memory servers. Multiple ways of installation
 are possible: as a stand-alone proxy, as a daemon executed by inetd or as
 a CGI program (deprecated). Client machines are configured by changing APT's
 proxy configuration or modification of access URLs in sources.list.
 .
 The package includes utilities to clean the cache (removing obsolete package
 files), generate usage reports and import existing package files.  Optional
 features include a file checksum verification framework, IPv6 support, FTP and
 HTTPS (proxying only) support as well as the simultaneous caching of
 repositories from different distributions.
 .
 Apt-cacher can be used as a replacement for apt-proxy, with no need to modify
 client's /etc/apt/sources.list files (and even reusing its config and cached
 data), or as an alternative to approx.
